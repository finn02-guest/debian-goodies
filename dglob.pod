=head1 NAME

dglob - Expand package names or files matching a pattern

=head1 SYNOPSIS

B<dglob> [B<-a>] I<pattern>

B<dglob> [B<-0>] B<-f> I<pattern>

=head1 DESCRIPTION

B<dglob> lists packages names matching a pattern. It can
also list all the files they contain. By default B<dglob> only searches
installed packages; the B<-a> and B<-n> switch widens the search (see L<"OPTIONS">). The
list is written to stdout, one name per line.

L<grep-dctrl(1)> and L<grep-aptavail(1)> are used to search the list of packages, so
you should refer to its documentation for information on how patterns are
matched.  By default, all packages whose name contains the given
string will be matched, but several options are available to modify
this behavior (see L<"OPTIONS">).

If you use dglob with the B<-f> option, all files in the matched packages
are listed instead of their names. If you do not use de B<-a> switch,
only existing, plain (i.e. no symlinks, directories or other special ones)
files are listed. If the B<-a> switch is use then all files will
be listed both for installed and non-installed packages. The filenames are
written to stdout, one file per line. You can use the B<-0> option to
get the filenames separated by '\0' instead of a newline.

=head1 OPTIONS

B<dglob> supports the following options:

=over 4

=item B<-a>

Search through all available packages, not just installed ones.
If set, L<grep-aptavail(1)> is used.

=item B<-A>

Do not emit architecture qualifiers on result.

=item B<-n>

Search through all the available packages but not including installed ones.
If set, L<grep-aptavail(1)> is used.


=item B<-f>

List all files in the matched packages. By default, this lists only installed
(i.e.  locally existing) files from installed packages. If used
together with B<-a> then it will list all files both of installed and
non-installed packages by mean of L<apt-file(1)>. If apt-file is not
installed, using B<-f> together with B<-a> is rather pointless.

=item B<-0>

When listing files (with B<-f>) use '\0' as a separator instead of
a newline. When specified without B<-f>, this options does nothing.

=item B<-r>, B<-e>, B<-i>, B<-X>, B<-v>

These options are passed directly to L<grep-dctrl(1)> or to L<grep-aptavail(1)> to
modify how the pattern is matched.  See L<grep-dctrl(1)>.

=back

=head1 EXAMPLES

 $ dglob libc6
 libc6-dbg:amd64
 libc6:amd64
 libc6:i386
 libc6-i386:amd64
 libc6-dev:amd64

 $ dglob libc6:amd64
 libc6-dbg:amd64
 libc6:amd64
 libc6-i386:amd64
 libc6-dev:amd64

 $ dglob zsh:all
 zsh-syntax-highlighting:all
 zsh-theme-powerlevel9k:all
 zsh-doc:all
 zsh-common:all
 zsh-antigen:all
 fizsh:all

 $ dglob -n libc6:i386
 libc6-amd64-dbgsym:i386
 libc6-amd64:i386
 libc6-dbg:i386
 libc6-dev-amd64:i386
 libc6-dev-x32:i386
 libc6-dev:i386
 libc6-pic:i386
 libc6-x32-dbgsym:i386
 libc6-x32:i386
 libc6-xen:i386

 $ dglob -a :amd64 | wc -l
 45846
 $ dglob -a :i386 | wc -l
 45185
 $ dglob -a :all | wc -l
 28995

=head1 FILES

=over 4

=item F</var/lib/dpkg/status>

L<dpkg(8)> status file, which serves as source for the list of available
and installed packages.

=back

=head1 AUTHORS

Matt Zimmerman <mdz@debian.org>
Axel Beckert <abe@debian.org>

This manpage was written by Frank Lichtenheld <frank@lichtenheld.de>,
and further enhanced by Javier Fernandez-Sanguino <jfs@debian.org>.

=head1 COPYRIGHT AND LICENCE

Copyright (C) 2001 Matt Zimmerman <mdz@debian.org>.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

On Debian systems, a copy of the GNU General Public License version 2
can be found in F</usr/share/common-licenses/GPL-2>.

=head1 SEE ALSO

L<grep-dctrl(1)>, L<grep-available(1)>, L<apt-file(1)>, L<dpkg(8)>
